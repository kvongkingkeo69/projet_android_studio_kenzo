package com.example.projet_mobile_kenzo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class InformationDataSource {
    private MySQLiteHelper db;
    private SQLiteDatabase dbHelper;

    //Constructeur qui permet de créer notre instance SQLiteOpenHelper
    public InformationDataSource(Context context) {

        db = new MySQLiteHelper(context);
    }

    //Méthode pour ouvrir l'accès à la BDD
    public void open() throws SQLException {
        dbHelper = db.getWritableDatabase();
    }

    // Méthode pour savoir sur quelle table on est
    public String info(){

        return db.TABLE_ETUDIANT;
    }

    //Méthode pour fermer la BDD


    //Méthode pour récupérer tous les films
    public ArrayList<Informations> getAllInformations(){
        ArrayList<Informations> listInfosEtudiants = new ArrayList<>();
        Cursor curseur = dbHelper.query(true,"Etudiant", new String[]{"nom_etu","pre_etu","mai_etu","tel_etu", "lib_cla", "lib_spe"},
                null,null,null,null,null,null);

        curseur.close();

        return listInfosEtudiants;
    }

    public void close() {

        dbHelper.close();
    }

    }


