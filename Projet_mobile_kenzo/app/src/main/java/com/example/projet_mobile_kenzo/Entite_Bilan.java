package com.example.projet_mobile_kenzo;

import java.util.Date;

public class Entite_Bilan {
    private Integer id_bil;
    private Date dat_bil;
    private float not_ent_bil;
    private float not_ora_bil;
    private float not_dos_bil;
    private float not_ora_fin_bil;
    private float not_dos_fin_bil;

    public Integer getId_bil() {
        return id_bil;
    }

    public void setId_bil(Integer id_bil) {
        this.id_bil = id_bil;
    }

    public Date getDat_bil() {
        return dat_bil;
    }

    public void setDat_bil(Date dat_bil) {
        this.dat_bil = dat_bil;
    }

    public float getNot_ent_bil() {
        return not_ent_bil;
    }

    public void setNot_ent_bil(float not_ent_bil) {
        this.not_ent_bil = not_ent_bil;
    }

    public float getNot_ora_bil() {
        return not_ora_bil;
    }

    public void setNot_ora_bil(float not_ora_bil) {
        this.not_ora_bil = not_ora_bil;
    }

    public float getNot_dos_bil() {
        return not_dos_bil;
    }

    public void setNot_dos_bil(float not_dos_bil) {
        this.not_dos_bil = not_dos_bil;
    }

    public float getNot_ora_fin_bil() {
        return not_ora_fin_bil;
    }

    public void setNot_ora_fin_bil(float not_ora_fin_bil) {
        this.not_ora_fin_bil = not_ora_fin_bil;
    }

    public float getNot_dos_fin_bil() {
        return not_dos_fin_bil;
    }

    public void setNot_dos_fin_bil(float not_dos_fin_bil) {
        this.not_dos_fin_bil = not_dos_fin_bil;
    }

    public Entite_Bilan(Integer id_bil, Date dat_bil, float not_ent_bil, float not_ora_bil, float not_dos_bil, float not_ora_fin_bil, float not_dos_fin_bil) {
        this.id_bil = id_bil;
        this.dat_bil = dat_bil;
        this.not_ent_bil = not_ent_bil;
        this.not_ora_bil = not_ora_bil;
        this.not_dos_bil = not_dos_bil;
        this.not_ora_fin_bil = not_ora_fin_bil;
        this.not_dos_fin_bil = not_dos_fin_bil;
    }

    @Override
    public String toString() {
        return "Bilan{" +
                "id_bil=" + id_bil +
                ", dat_bil=" + dat_bil +
                ", not_ent_bil=" + not_ent_bil +
                ", not_ora_bil=" + not_ora_bil +
                ", not_dos_bil=" + not_dos_bil +
                ", not_ora_fin_bil=" + not_ora_fin_bil +
                ", not_dos_fin_bil=" + not_dos_fin_bil +
                '}';
    }
}
