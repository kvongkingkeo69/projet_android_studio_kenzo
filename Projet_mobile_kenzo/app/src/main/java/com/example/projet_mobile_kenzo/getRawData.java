package com.example.projet_mobile_kenzo;
import android.os.AsyncTask;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class getRawData {

    // url de récupération de données json sur le site flickr
    private String mRawUrl;

    // fn+alt+insert pour constructeur automatique
    public getRawData(String url) {
        mRawUrl = url;
    }

    public void startDownload(){
        DownloadRawData downloadRawData = new DownloadRawData();
        downloadRawData.execute(mRawUrl);
    }
    class DownloadRawData extends AsyncTask<String,Void,String> {
        // AsyncTask --> tache exécutée en arrière plan
        // String : paramètre(s), Void : progrès (type non utilisé Void), String : résultat
        @Override
        protected void onPostExecute(String result) {
            // permet de recupérer le resultat du téléchargement
            super.onPostExecute(result);
            Log.d("DownloadRawData",result);
        }

        protected String doInBackground(String... params) {
            //invoqué sur le thread en arrière plan après le OnPreExecute()
            // etape permettant d'effectuer la tache de fond qui peut prendre un certain temps
            // les paramètres sont passés et le résultat doit être retourné
            HttpURLConnection urlConnection=null;
            BufferedReader reader= null;
            StringBuffer buffer=null;
            try {
                URL url = new URL(params[0]); // params[0] car 1 seul paramètre
                urlConnection= (HttpURLConnection) url.openConnection(); // ouverture de la connexion
                InputStream inputStream=urlConnection.getInputStream(); // récupération du flux
                if(inputStream==null){ // si la connexion a échoué
                    return null;
                }
                reader=new BufferedReader(new InputStreamReader(inputStream));
                String line;
                buffer = new StringBuffer();
                while((line=reader.readLine())!=null){ // tant qu'il y a une donnée dans le reader
                    buffer.append(line+"\n"); // retour à la ligne après chaque ligne
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if(urlConnection!=null){
                    urlConnection.disconnect();; // lorsque la connexion n'est plus requise
                }
                if(reader!=null){
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return buffer.toString();
        }
    }
}



