package com.example.projet_mobile_kenzo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLiteHelper extends SQLiteOpenHelper
{
    private static final String DATABASE_NAME = "projet_mobile.db";
    private static final int DATABASE_VERSION = 1;
    public static final String TABLE_ETUDIANT = "Etudiant";
    public static final String TABLE_BILAN = "Bilan";






    // Commande sql pour la création de la base de données


    private static final String DATABASE_CREATE_ETUDIANT = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ETUDIANT + "( num_etu integer primary key autoincrement, " +
            "num_etu auto_increment," +
            "nom_etu text, " +
            "pre_etu text, " +
            "tel_etu int, " +
            "mai_etu text," +
            "log_etu text, " +
            "mdp_etu text," +
            "libelle_note text," +
            "lib_spe text," +
            "lib_cla text," +
            "lib_sta text);";

    private static final String DATABASE_CREATE_BILAN = "CREATE TABLE IF NOT EXISTS "
            + TABLE_BILAN + "( id_bil integer primary key autoincrement, " +
            "id_bil auto_increment," +
            "dat_bil date, " +
            "not_ent_bil text, " +
            "not_ora_bil int, " +
            "not_dos_bil text," +
            "not_ora_fin_bil text, " +
            "not_dos_fin_bil text);";





    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open() throws SQLException {
        this.getWritableDatabase();
    }

    public void onCreate (SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE_ETUDIANT) ;
        db.execSQL(DATABASE_CREATE_BILAN);

    }
    @Override
    public void onUpgrade (SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists Etudiant");
        db.execSQL("drop table if exists Bilan");
        Log.w(MySQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        onCreate(db);


    }

    public Boolean verifLogin_Mdp(String log_etu, String mdp_etu) {
        SQLiteDatabase db= this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from Etudiant where log_etu=? and mdp_etu=?", new String[] {log_etu, mdp_etu});
        if(cursor.getCount()>0)
            return true;
        else
            return false;
    }
}