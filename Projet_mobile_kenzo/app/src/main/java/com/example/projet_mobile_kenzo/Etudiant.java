package com.example.projet_mobile_kenzo;

import java.io.Serializable;

public class Etudiant implements Serializable {
    private int num_etu;
    private String nom_etu;
    private String pre_etu;
    private String tel_etu;
    private String mai_etu;
    private String log_etu;
    private String mdp_etu;
    private String libelle_note;
    private Integer nb_notes;


    public int getNum_etu() {
        return num_etu;
    }

    public String getNom_etu() {
        return nom_etu;
    }

    public String getPre_etu() {
        return pre_etu;
    }

    public String getTel_etu() {
        return tel_etu;
    }

    public String getMai_etu() {
        return mai_etu;
    }

    public String getLog_etu() {
        return log_etu;
    }

    public String getMdp_etu() {
        return mdp_etu;
    }

    public Integer getNb_notes() {
        return nb_notes;
    }

    public String getLibelle_note() {
        return libelle_note;
    }

    public void setLibelle_note(String libelle_note) {
        this.libelle_note = libelle_note;
    }

    public void setNum_etu(int num_etu) {
        this.num_etu = num_etu;
    }

    public void setNom_etu(String nom_etu) {
        this.nom_etu = nom_etu;
    }

    public void setPre_etu(String pre_etu) {
        this.pre_etu = pre_etu;
    }

    public void setTel_etu(String tel_etu) {
        this.tel_etu = tel_etu;
    }

    public void setMai_etu(String mai_etu) {
        this.mai_etu = mai_etu;
    }

    public void setLog_etu(String log_etu) {
        this.log_etu = log_etu;
    }

    public void setMdp_etu(String mdp_etu) {
        this.mdp_etu = mdp_etu;
    }

    public void setNb_notes(Integer nb_notes) {
        this.nb_notes = nb_notes;
    }

    public Etudiant(int num_etu, String nom_etu, String pre_etu, String tel_etu, String mai_etu, String log_etu, String mdp_etu, String libelle_note, Integer nb_notes) {
        this.num_etu = num_etu;
        this.nom_etu = nom_etu;
        this.pre_etu = pre_etu;
        this.tel_etu = tel_etu;
        this.mai_etu = mai_etu;
        this.log_etu = log_etu;
        this.mdp_etu = mdp_etu;
        this.libelle_note = libelle_note;
        this.nb_notes = nb_notes;
    }


    @Override
    public String toString() {
        return "Etudiant{" +
                "num_etu=" + num_etu +
                ", nom_etu='" + nom_etu + '\'' +
                ", pre_etu='" + pre_etu + '\'' +
                ", tel_etu='" + tel_etu + '\'' +
                ", mai_etu='" + mai_etu + '\'' +
                ", log_etu='" + log_etu + '\'' +
                ", mdp_etu='" + mdp_etu + '\'' +
                ", libelle_note='" + libelle_note + '\'' +
                ", nb_notes=" + nb_notes +
                '}';
    }
}
