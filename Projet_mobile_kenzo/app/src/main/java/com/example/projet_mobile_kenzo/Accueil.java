package com.example.projet_mobile_kenzo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

import java.sql.SQLException;


public class Accueil extends AppCompatActivity {
    private Button buttonInfos;
    private Button buttonBilans;
    private MySQLiteHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accueil);
        init();
        db = new MySQLiteHelper(this);




    }

    private void init() {
        Intent intent = getIntent();
        buttonInfos = (Button) findViewById(R.id.buttonInfos);
        buttonBilans = (Button) findViewById(R.id.buttonBilans);

        buttonBilans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Accueil.this, Bilans.class));

            }
        });

        buttonInfos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Accueil.this, Informations.class));
            }
        });


    }

}
