package com.example.projet_mobile_kenzo;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.List;

public class Informations extends AppCompatActivity {
    private Button buttonRetour;
    private ListView listViewInfos;
    private InformationDataSource dataSource;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.informations);
        dataSource = new InformationDataSource(this);
        dataSource.open();
        List<Informations> lesInfos = dataSource.getAllInformations();
        listViewInfos = (ListView) findViewById(R.id.listViewInformations);
        ArrayAdapter<Informations> adapter = new ArrayAdapter<Informations>(this, android.R.layout.simple_list_item_1, lesInfos);
        listViewInfos.setAdapter(adapter);

        initialisation();
    }

    private void initialisation() {
        buttonRetour = (Button) findViewById(R.id.buttonRetour);
        listViewInfos = (ListView) findViewById(R.id.listViewInformations);

        buttonRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Informations.this, Accueil.class));
            }
        });
    }


}


