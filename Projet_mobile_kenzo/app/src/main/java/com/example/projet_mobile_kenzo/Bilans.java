package com.example.projet_mobile_kenzo;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.sql.SQLException;

public class Bilans extends AppCompatActivity {

    private ListView listViewBilan1;
    private ListView listViewBilan2;
    private Button buttonRetour;
    private MySQLiteHelper db;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bilan);
        initialisation();
        db = new MySQLiteHelper(this);
         // db.open();

    }

    private void initialisation() {
        Intent intent = getIntent();
        buttonRetour = findViewById(R.id.buttonRetour);


        buttonRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Bilans.this, Accueil.class));
            }
        });


    }

}
